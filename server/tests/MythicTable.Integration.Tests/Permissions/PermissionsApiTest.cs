using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Permissions.Data;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Permissions;

public class PermissionsApiTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    [Fact]
    public async Task RequiresAuthTest()
    {
        var response = await UnauthenticatedClient.GetAsync("api/permissions/campaign_id");

        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }

    [Fact]
    public async Task ReturnsEmptyList()
    {
        await ProfileTestUtil.Login(Client);

        var response = await Client.GetAsync("api/permissions/campaign_id");

        response.EnsureSuccessStatusCode();

        var json = await response.Content.ReadAsStringAsync();
        var result = JsonConvert.DeserializeObject<List<PermissionsDto>>(json);
        Assert.Equal(new List<PermissionsDto>(), result);
    }
}