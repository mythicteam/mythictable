using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace MythicTable.Integration.Tests.Helpers;

public class WebApiTestSuite(MongoDbFixture fixture) : MongoDbTestSuite(fixture)
{
    public TestServer Server { get; private set; }
    public HttpClient Client { get; private set; }
    public HttpClient UnauthenticatedClient { get; private set; }

    public override async Task InitializeAsync()
    {
        await base.InitializeAsync();

        var builder = new WebHostBuilder()
            .UseSetting("MTT_MONGODB_CONNECTIONSTRING", MongoSettings.ConnectionString)
            .UseSetting("MTT_MONGODB_DATABASENAME", MongoSettings.DatabaseName)
            .UseStartup<TestStartup>();
        Server = new TestServer(builder);
        Client = Server.CreateClient();
        Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "any-token-will-work");
        UnauthenticatedClient = Server.CreateClient();
    }

    public override Task DisposeAsync()
    {
        Client.Dispose();
        return base.DisposeAsync();
    }

    protected HttpClient CreateClient(string userId, string username)
    {
        var client = Server.CreateClient();
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "any-token-will-work");
        TestStartup.SetUpClientForUser(client, userId, username);
        return client;
    }
}
