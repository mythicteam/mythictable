using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using static System.String;

namespace MythicTable.Integration.Tests.Helpers
{
    public class TestStartup(IWebHostEnvironment environment, IConfiguration configuration)
        : Startup(environment, configuration)
    {
        public const string FakeUserIdHeader = "FakeUserId";
        public const string FakeUserNameHeader = "FakeUserName";
        public const string FakeGroupHeader = "FakeGroupClaims";

        protected override void ConfigureAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Test"; // has to match scheme in TestAuthenticationExtensions
                options.DefaultChallengeScheme = "Test";
            })
            .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", _ => { });
        }

        public static void SetUpClientForUser(HttpClient client, string userId, string username)
        {
            client.DefaultRequestHeaders.Add(TestStartup.FakeUserIdHeader, userId);
            client.DefaultRequestHeaders.Add(TestStartup.FakeUserNameHeader, username);
        }
    }

    public class TestAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        [Obsolete("Depends on obsolete ISystemClock but is still required by AuthenticationSchemeOptions")]
        public TestAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        public TestAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder) : base(options, logger, encoder)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!IsAuthorized())
            {
                return Task.FromResult(AuthenticateResult.Fail(Empty));
            }

            var claims = new List<Claim>
            {
                new(ClaimTypes.NameIdentifier, GetTestUserId()),
                new("preferred_username", GetTestUserName()),
            };

            claims.AddRange(GetGroupClaims());

            var identity = new ClaimsIdentity(claims, "Test");
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, "Test");

            var result = AuthenticateResult.Success(ticket);

            return Task.FromResult(result);
        }

        private bool IsAuthorized()
        {
            return Request.Headers.ContainsKey("Authorization");
        }

        private List<Claim> GetGroupClaims()
        {
            var result = new List<Claim>();
            if (Request.Headers.ContainsKey(TestStartup.FakeGroupHeader))
            {
                var groups = JsonConvert.DeserializeObject<List<string>>(
                    Request.Headers[TestStartup.FakeGroupHeader]
                );
                foreach(var group in groups) 
                {
                    result.Add(new Claim("groups", group));
                }
            }
            return result;
        }

        private string GetTestUserId()
        {
            if (Request.Headers.ContainsKey(TestStartup.FakeUserIdHeader))
            {
                return Request.Headers[TestStartup.FakeUserIdHeader];
            }
            return "Test user";
        }

        private string GetTestUserName()
        {
            if (Request.Headers.ContainsKey(TestStartup.FakeUserNameHeader))
            {
                return Request.Headers[TestStartup.FakeUserNameHeader];
            }
            return "test@user.me";
        }
    }
}