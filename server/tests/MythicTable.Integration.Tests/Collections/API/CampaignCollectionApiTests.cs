using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Humanizer;
using MythicTable.Campaign.Data;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.API;

public class CampaignCollectionApiTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    private const string CollectionName = "collection_1";
    private const string CampaignId = "4dad901291c2949e7a5b6aa8";

    private const string BaseUrl = "/api/collections";
    private HttpRequestInfo RqInfoGet => new() { Method = HttpMethod.Get, Url = BaseUrl };
    private HttpRequestInfo RqInfoPost => new() { Method = HttpMethod.Post, Url = BaseUrl };

    [Fact]
    public async Task RequiresCampaign()
    {
        var rqInfo = RqInfoGet;
        rqInfo.Url += $"/{CollectionName}/campaign/{CampaignId}";

        using var response = await Client.MakeRequest(rqInfo);

        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }

    [Fact]
    public async Task GetCollectionReturnsEmpty()
    {
        await ProfileTestUtil.Login(Client);

        var campaign = await CreateCampaign(new CampaignDTO());

        List<JObject> jObjects = await GetCollection(campaign);
        Assert.Empty(jObjects);
    }

    [Fact]
    public async Task CollectionsAreCampaignSpecific()
    {
        await ProfileTestUtil.Login(Client);

        var campaign1 = await CreateCampaign(new CampaignDTO());
        var campaign2 = await CreateCampaign(new CampaignDTO());

        await CreateCampaignObject(campaign1, new JObject());

        Assert.Single(await GetCollection(campaign1));
        Assert.Empty(await GetCollection(campaign2));
    }

    [Fact]
    public async Task CampaignCollectionsAreNotUserSpecific()
    {
        await ProfileTestUtil.Login(Client);

        var campaign = await CreateCampaign(new CampaignDTO());
        
        var client = CreateClient("fake-user-02", "fake-user-02");
        await ProfileTestUtil.Login(client);
        await AddPlayer(client, campaign);
            
        await CreateCampaignObject(campaign, new JObject());

        Assert.Single(await GetCollection(campaign));

        var response = await client.GetAsync($"{BaseUrl}/{CollectionName}/campaign/{campaign.Id}");

        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        var collections = JsonConvert.DeserializeObject<List<JObject>>(json);

        Assert.Single(collections);
    }

    private async Task<CampaignDTO> CreateCampaign(CampaignDTO campaign)
    {
        var rqInfo = RqInfoPost;
        rqInfo.Url = "/api/campaigns";
        rqInfo.Content = campaign;

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<CampaignDTO>(json);
    }

    private async Task<List<JObject>> GetCollection(CampaignDTO campaign)
    {
        var rqInfo = RqInfoGet;
        rqInfo.Url += $"/{CollectionName}/campaign/{campaign.Id}";

        using var response = await Client.MakeRequest(rqInfo);
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<JObject>>(json);
    }

    private async Task CreateCampaignObject(CampaignDTO campaign, JObject jObject)
    {
        var rqInfo = RqInfoPost;
        rqInfo.Url += $"/{CollectionName}/campaign/{campaign.Id}";
        rqInfo.Content = jObject;

        using var response = await Client.MakeRequest(rqInfo);
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        JsonConvert.DeserializeObject<JObject>(json);
    }

    private async Task AddPlayer(HttpClient client, CampaignDTO campaign)
    {
        var response = await client.PutAsync($"/api/campaigns/join/{campaign.JoinId}", null);

        response.EnsureSuccessStatusCode();

        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        JsonConvert.DeserializeObject<CampaignDTO>(json);
    }
}