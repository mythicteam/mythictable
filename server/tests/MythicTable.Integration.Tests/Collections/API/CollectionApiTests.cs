using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.API;

public class CollectionApiTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    private const string BaseUrl = "/api/collections";
    static HttpRequestInfo RqInfoDelete => new() { Method = HttpMethod.Delete, Url = BaseUrl };
    static HttpRequestInfo RqInfoGet => new() { Method = HttpMethod.Get, Url = BaseUrl };
    static HttpRequestInfo RqInfoPost => new() { Method = HttpMethod.Post, Url = BaseUrl };
    static HttpRequestInfo RqInfoPut => new() { Method = HttpMethod.Put, Url = BaseUrl };

    [Fact]
    public async Task GetCollectionReturnsEmpty()
    {
        await ProfileTestUtil.Login(Client);
        var rqInfo = RqInfoGet;
        rqInfo.Url += "/test";

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        var jObjects = JsonConvert.DeserializeObject<List<JObject>>(json);
        Assert.Empty(jObjects);
    }

    [Fact]
    public async Task PostAndGetReturnsJObjects()
    {
        await ProfileTestUtil.Login(Client);

        var o = new JObject
        {
            { "Name", "Integration Test JObject" }
        };
        var collection = "test";

        await CreateObjectInCollection(collection, o);

        List<JObject> jObjects = await GetCollection(collection);
        Assert.Single(jObjects);
        Assert.Equal("Integration Test JObject", jObjects[0]["Name"]);
    }

    [Fact]
    public async Task UpdateChangesJObjects()
    {
        await ProfileTestUtil.Login(Client);

        var o = new JObject
        {
            { "Name", "Integration Test JObject" }
        };
        const string collection = "test";

        var jObject = await CreateObjectInCollection(collection, o);

        var patch = new JsonPatchDocument()
            .Replace("Name", "Update Test JObject")
            .Add("foo", "bar");
        jObject = await UpdateObjectInCollection(collection, patch, jObject["_id"]?.ToString());

        Assert.Equal("Update Test JObject", jObject["Name"]);
        Assert.Equal("bar", jObject["foo"]);
    }

    [Fact]
    public async Task DeleteApi()
    {
        await ProfileTestUtil.Login(Client);

        var collection = "test";
        var jObject = await CreateObjectInCollection(collection, new JObject());

        await DeleteObjectInCollection(collection, jObject["_id"]?.ToString());

        List<JObject> jObjects = await GetCollection(collection);
        Assert.Empty(jObjects);
    }

    private async Task<JObject> CreateObjectInCollection(string collection, JObject o)
    {
        var rqInfo = RqInfoPost;
        rqInfo.Url += $"/{collection}";
        rqInfo.Content = o;

        using var response = await Client.MakeRequest(rqInfo);
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<JObject>(json);
    }

    private async Task<List<JObject>> GetCollection(string collection)
    {
        var rqInfo = RqInfoGet;
        rqInfo.Url += $"/{collection}";

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<JObject>>(json);
    }

    private async Task<JObject> UpdateObjectInCollection(string collection, JsonPatchDocument patch, string id)
    {
        var rqInfo = RqInfoPut;
        rqInfo.Url += $"/{collection}/id/{id}";
        rqInfo.Content = patch;

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<JObject>(json);
    }

    private async Task DeleteObjectInCollection(string collection, string id)
    {
        var rqInfo = RqInfoDelete;
        rqInfo.Url += $"/{collection}/id/{id}";

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
    }
}