using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;
using MythicTable.Integration.Tests.Helpers;

namespace MythicTable.Integration.Tests.Campaign;

public class MultiUserCampaignTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    private const string BaseUrl = "/api/campaigns";
    private static HttpRequestInfo RqInfoPost => new() { Method = HttpMethod.Post, Url = BaseUrl };

    [Fact]
    public async Task UnauthorizedCampaignDeleteReturns401()
    {
        await ProfileTestUtil.Login(Client);

        var campaign = new CampaignDTO() { Name = "Integration Test Campaign" };
        var rqInfo = RqInfoPost;
        rqInfo.Content = campaign;

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();

        var json = await response.Content.ReadAsStringAsync();
        var campaignObject = JsonConvert.DeserializeObject<CampaignDTO>(json);

        var client = CreateClient("fake-user-02", "fake-user-02");
        await ProfileTestUtil.Login(client);

        using var deleteResponse = await client.DeleteAsync($"{BaseUrl}/{campaignObject.Id}");
        Assert.Equal(HttpStatusCode.Unauthorized, deleteResponse.StatusCode);
    }

    [Fact]
    public async Task UnauthorizedCampaignUpdateReturns401()
    {
        await ProfileTestUtil.Login(Client);

        var campaign = new CampaignDTO()
        {
            Name = "Integration Test Campaign"
        };
            
        using var campaignResponse = await Client.MakeRequest(new HttpRequestInfo{
            Method = HttpMethod.Post,
            Url = "/api/campaigns",
            Content = campaign,
        });
        campaignResponse.EnsureSuccessStatusCode();

        var json = await campaignResponse.Content.ReadAsStringAsync();
        campaign = JsonConvert.DeserializeObject<CampaignDTO>(json);

        var updatedCampaign = new CampaignDTO()
        {
            Name = "Failed Integration Test"
        };
        Client.DefaultRequestHeaders.Add(TestStartup.FakeUserIdHeader, "fake-user-02");
        using var profileCreate = await Client.MakeRequest(new HttpRequestInfo{
            Method = HttpMethod.Get,
            Url = "/api/profiles/me"
        });
        profileCreate.EnsureSuccessStatusCode();
        using var unauthorizedResponse = await Client.MakeRequest(new HttpRequestInfo{
            Method = HttpMethod.Put,
            Url = $"/api/campaigns/{campaign.Id}",  
            Content = updatedCampaign
        });
        Assert.Equal(HttpStatusCode.Unauthorized, unauthorizedResponse.StatusCode);
    }
}