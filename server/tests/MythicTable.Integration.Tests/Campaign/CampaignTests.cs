using System.Threading.Tasks;
using System.Net;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;
using System.Net.Http;
using MythicTable.Integration.Tests.Helpers;

namespace MythicTable.Integration.Tests.Campaign;

public class CampaignTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
{
    private const string BaseUrl = "/api/campaigns";
    HttpRequestInfo RqInfoDelete => new() { Method = HttpMethod.Delete, Url = BaseUrl };
    HttpRequestInfo RqInfoPost => new() { Method = HttpMethod.Post, Url = BaseUrl };

    [Fact]
    public async Task CreateCampaignTest()
    {
        await ProfileTestUtil.Login(Client);

        var campaign = new CampaignDTO()
        {
            Name = "Integration Test Campaign"
        };

        using var response = await Client.MakeRequest(new HttpRequestInfo() 
        { 
            Method = HttpMethod.Post,
            Url="/api/campaigns", 
            Content = campaign 
        });
        response.EnsureSuccessStatusCode();
        Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType?.ToString());
        var json = await response.Content.ReadAsStringAsync();
        var newCampaign = JsonConvert.DeserializeObject<CampaignDTO>(json);
        Assert.Equal("Integration Test Campaign", newCampaign.Name);
    }

    [Fact]
    public async Task AuthorizedCampaignDeleteTest() 
    {
        await ProfileTestUtil.Login(Client);

        var campaign = new CampaignDTO() { Name = "Integration Test Campaign" };

        var rqInfo = RqInfoPost;
        rqInfo.Content = campaign;

        using var response = await Client.MakeRequest(rqInfo);

        response.EnsureSuccessStatusCode();
        var json = await response.Content.ReadAsStringAsync();
        var campaignObject = JsonConvert.DeserializeObject<CampaignDTO>(json);

        rqInfo = RqInfoDelete;
        rqInfo.Url += $"/{campaignObject.Id}";
        using var deleteResponse = await Client.MakeRequest(rqInfo);
        deleteResponse.EnsureSuccessStatusCode();

        rqInfo.Method = HttpMethod.Get;
        using var getResponse = await Client.MakeRequest(rqInfo);
        Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
    }
}