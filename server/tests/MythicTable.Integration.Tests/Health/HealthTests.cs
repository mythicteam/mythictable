using System.Threading.Tasks;
using MythicTable.Integration.Tests.Helpers;
using Xunit;

namespace MythicTable.Integration.Tests.Health
{
    public class HealthTests(MongoDbFixture fixture) : WebApiTestSuite(fixture)
    {
        [Fact]
        public async Task HealthTest()
        {
            var response = await Client.GetAsync("healthz");
            response.EnsureSuccessStatusCode();
        }
    }
}
