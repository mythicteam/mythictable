﻿using MythicTable.Library.Controller;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerLibraryTests: LibraryControllerBase
    {
        [Fact]
        public async Task ReturnsEmptyList()
        {
            var results = await Controller.Get("/library/");
            Empty(results);
            results = await Controller.Get("/library/path/to/nowhere");
            Empty(results);
        }

        [Fact]
        public async Task ReturnNonCampaignCollections()
        {
            SetupCampaignCollections(
                (string)null,
                CreateObject(1, null),
                CreateObject(2, null),
                CreateObject(3, null)
            );
            var results = await Controller.Get("/library/");
            Equal(3, results.Count);
        }

        [Fact]
        public async Task ReturnsOnlyCollectionsInPath()
        {
            SetupCampaignCollections(
                (string)null,
                CreateObject(1, null)
            );
            SetupCampaignCollections(
                (string)null, "foo",
                CreateObject(2, null, "foo"),
                CreateObject(3, null, "foo"),
                CreateObject(4, null, "foo")
            );
            SetupCampaignCollections(
                (string)null, "bar",
                CreateObject(5, null, "bar"),
                CreateObject(6, null, "bar")
            );
            SetupCampaignCollections(
                (string)null, "foo/bar",
                CreateObject(7, null, "foo/bar")
            );
            Single(await Controller.Get("/library/"));
            Equal(3, (await Controller.Get("/library/foo")).Count);
            Equal(2, (await Controller.Get("/library/bar")).Count);
            Single(await Controller.Get("/library/foo/bar"));
        }

        [Fact]
        public async Task ReturnsLibraryData()
        {
            SetupCampaignCollections(
                (string)null,
                CreateObject(1, null, null, new JObject
                {
                    { "hp", 30 },
                    { "name", "Sam" }
                })
            );
            var results = await Controller.Get("/library/");
            Single(results);
            var sam = results[0];
            Equal("1", sam.Id);
            Equal("character", sam.Collection);
        }

        [Fact]
        public async Task ReturnsLibraryDataInFolder()
        {
            SetupCampaignCollections(
                (string)null, "shire/hobbits",
                CreateObject(1, null, "shire/hobbits", new JObject
                {
                    { "hp", 30 },
                    { "name", "Pippin" }
                })
            );
            var results = await Controller.Get("/library/shire/hobbits/");
            Single(results);
            var pippin = results[0];
            Equal("1", pippin.Id);
            Equal("shire/hobbits", pippin.Path);
            Equal("character", pippin.Collection);
        }

        [Fact]
        public async Task BasicRecursion()
        {
            SetupCampaignCollections(
                (string)null,
                CreateFolder(1, "folder1", null, null)
            );
            SetupCampaignCollections(
                (string)null, "folder1",
                CreateFolder(2, "folder2", null, null)
            );
            SetupCampaignCollections(
                (string)null, "folder1/folder2",
                CreateFolder(3, "folder3", null, null)
            );

            var remainingContents = await Controller.Get("/library", recursive: true);
            Single(remainingContents);
            Equal("folder1", remainingContents[0].Name);
            Single(remainingContents[0].Children);
            Equal("folder2", remainingContents[0].Children[0].Name);
            Single(remainingContents[0].Children[0].Children);
            Equal("folder3", remainingContents[0].Children[0].Children[0].Name);
        }
    }
}
