﻿using System.Threading.Tasks;
using MythicTable.Campaign.Data;

namespace MythicTable.Collections.Services
{
    public interface ICollectionPermissionsService
    {
        Task AssertUserPermission(CollectionActionRequest permissionRequest);
    }
}