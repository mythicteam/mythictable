﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.IdentityModel.Tokens;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Providers;
using Newtonsoft.Json.Linq;

namespace MythicTable.Library.Controller
{
    public class LibraryPath
    {
        public enum RootFolder
        {
            Root = -1,
            Campaigns,
            Library,
            Public
        }

        public static readonly string[] RootFolders = new string[]
        {
            "campaigns",
            "library",
            "public"
        };

        public static string ValidChars = "a-zA-Z0-9-_";

        public List<string> Folders { get; } = new List<string>();
        public RootFolder Root { get; }

        public bool IsCampaignRoot => Root == RootFolder.Campaigns && Campaign == null;
        public string Campaign { get; }

        public LibraryPath(string path)
        {
            if (!ValidatePath(path))
            {
                throw new LibraryException($"Invalid path '{path}'");
            }
            var folders = GetFolders(path);
            if (folders.IsNullOrEmpty())
            {
                Root = RootFolder.Root;
                return;
            }
            Root = (RootFolder)Array.FindIndex(RootFolders,s => s == folders[0]);
            if (Root == RootFolder.Campaigns)
            {
                if (folders.Length < 2) return;
                Campaign = folders[1];
                Folders = folders.Skip(2).ToList();

            }
            else if (folders.Length != 0)
            {
                Folders = folders.Skip(1).ToList();
            }

        }

        private static bool ValidatePath(string path)
        {
            if (path == "/")
            {
                return true;
            }
            if(GetFolders(path).Any(string.IsNullOrEmpty))
            {
                return false;
            }
            var pattern = $"^/({string.Join("|", RootFolders)})(/[{ValidChars}]*)*$";
            return Regex.IsMatch(path, pattern);
        }

        private static string[] GetFolders(string path)
        {
            return path == "/" ? Array.Empty<string>() : path.Trim('/').Split('/');
        }

        public static bool IsRecursive(JObject jObject, string campaignId, string folders)
        {
            if (jObject.GetCampaignId() != campaignId)
            {
                return false;
            }
            if (folders.IsNullOrEmpty())
            {
                return false;
            }

            var path =  (jObject.GetPath().IsNullOrEmpty() ? "" : jObject.GetPath() + "/") + jObject.GetName();
            return folders.StartsWith(path);
        }

        public string GetParentPath()
        {
            if (Root == RootFolder.Root)
            {
                throw new LibraryException("Cannot get parent of root");
            }
            if (Folders.IsNullOrEmpty())
            {
                throw new LibraryException("Cannot get parent when there are no folders");
            }
            List<string> path = new List<string> { RootFolders[(int)Root] };
            if (Root == RootFolder.Campaigns)
            {
                path.Add(Campaign);
            }
            path.AddRange(Folders.Take(Folders.Count - 1));
            return "/" + string.Join("/", path);
        }

        public static string SlugifyCampaignName(string name)
        {
            return new string(name.Select(c => Regex.IsMatch(c.ToString(), $"[{LibraryPath.ValidChars}]") ? c : '_').ToArray());
        }
    }
}