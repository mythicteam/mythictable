﻿using MythicTable.Common.Exceptions;

namespace MythicTable.Library.Controller
{
    public class LibraryException: MythicTableException
    {
        public LibraryException(string msg) : base(msg)
        {
        }
    }
}