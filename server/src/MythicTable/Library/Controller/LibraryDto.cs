﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using MythicTable.Collections.Providers;

namespace MythicTable.Library.Controller
{
    public class LibraryDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Campaign { get; set; }
        public string Collection { get; set; }
        public List<LibraryDto> Children { get; set; }

        public bool IsFolder => Collection == "folder";

        public LibraryDto()
        {
        }

        public LibraryDto(JObject obj)
        {
            Id = obj.GetId();
            Name = FindName(obj);
            Path = obj.GetPath();
            Campaign = obj.GetCampaignId();
            Collection = obj.GetCollection();
        }

        private static string FindName(JObject obj)
        {
            if (obj.ContainsKey("_name"))
            {
                return obj["_name"].ToString();
            }
            if (obj.GetValue("name", StringComparison.OrdinalIgnoreCase) != null)
            {
                return obj.GetValue("name", StringComparison.OrdinalIgnoreCase)?.Value<string>();
            }
            return obj["_id"].ToString();
        }
    }
}